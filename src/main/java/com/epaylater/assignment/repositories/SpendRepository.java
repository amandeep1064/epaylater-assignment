package com.epaylater.assignment.repositories;

import com.epaylater.assignment.entities.Spend;
import org.springframework.data.repository.CrudRepository;

public interface SpendRepository extends CrudRepository<Spend, String> {

}
