package com.epaylater.assignment.repositories;

import com.epaylater.assignment.entities.EPayLaterUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<EPayLaterUser, String> {

    @Query(value = "Update epay_user set credit_used = credit_used + ?1 where phone_number = ?2",nativeQuery = true)
    public void updateCreditUsed(Float creditUsed,String phoneNumber);
}
