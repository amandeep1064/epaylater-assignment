package com.epaylater.assignment.exceptions;

public class NotEnoughCreditException extends Exception {

    public NotEnoughCreditException(String message) {
        super(message);
    }
}
