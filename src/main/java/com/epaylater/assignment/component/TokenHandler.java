package com.epaylater.assignment.component;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public final class TokenHandler {

    @Autowired
    private EPayUserDetailService userService;

    @Value("${security.signing-key}")
    private String secretSigningKey;


    public UserDetails parseUserFromToken(String token) {
        String username = Jwts.parser().setSigningKey(secretSigningKey.trim()).parseClaimsJws(token).getBody().getSubject();
        return userService.loadUserByUsername(username);
    }

    public String createTokenForUser(UserDetails user, HttpServletRequest request) {
        Date date = new Date();
        LocalDateTime time = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        time = time.plusMinutes(15);
        Date expDate = Date.from(time.toInstant(ZoneOffset.UTC));
        Map<String, Object> claims = new HashMap<>();
        claims.put("jti", UUID.randomUUID().toString());
        claims.put("sub",user.getUsername());
        JwtBuilder builder = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secretSigningKey)
                .setExpiration(expDate);
        return builder.compact();
    }
}