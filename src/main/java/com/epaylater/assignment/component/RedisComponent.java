package com.epaylater.assignment.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Component
public class RedisComponent {


    private JedisPool jedisPool ;

    @Value("${redis.host}")
    private String host;

    @Value("${redis.port}")
    private Integer port;

    @Bean
    public JedisPool makePool() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(500);
        config.setMaxIdle(100);
        config.setMinIdle(10);
        config.setMaxWaitMillis(30000);
        this.jedisPool = new JedisPool(config, host, port);
        return jedisPool;
    }

    public Jedis getJedisInstance() {
        return jedisPool.getResource();
    }

}
