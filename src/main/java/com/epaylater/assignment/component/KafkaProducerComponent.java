package com.epaylater.assignment.component;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
public class KafkaProducerComponent {

    @Autowired
    private KafkaProducer<String, byte[]> kafkaProducer;
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerComponent.class);


    @Bean
    private KafkaProducer<String, byte[]> createKafkaProducer() {
        try {
            InputStream inputStream = new ClassPathResource("kafka.properties").getInputStream();
            Properties properties = new Properties();
            properties.load(inputStream);
            return new KafkaProducer<String, byte[]>(properties);
        } catch (IOException exception) {
            LOGGER.error("Error loading properties {} ", exception);
            LOGGER.error("Shutting down. Ensure that kafka properties are read properly");
            System.exit(-1);
        }
        return null;
    }

    public KafkaProducer<String, byte[]> getKafkaProducer() {
        return kafkaProducer;
    }
}
