package com.epaylater.assignment.component;

import com.epaylater.assignment.entities.EPayLaterUser;
import com.epaylater.assignment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class EPayUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        EPayLaterUser epayLaterUser = userRepository.findOne(s);
        if (epayLaterUser == null)
            throw new SecurityException("Invalid Phone Number ");
        return epayLaterUser;
    }

}
