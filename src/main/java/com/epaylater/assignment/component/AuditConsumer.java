package com.epaylater.assignment.component;

import com.epaylater.assignment.entities.EPayLaterUser;
import com.epaylater.assignment.entities.Spend;
import com.epaylater.assignment.repositories.SpendRepository;
import com.epaylater.assignment.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class AuditConsumer {
    @Value("${audit.topic}")
    private String topic;

    private ExecutorService executorService;

    private AtomicBoolean isStopped = new AtomicBoolean(false);

    @Autowired
    private KafkaConsumer<String, byte[]> consumer;

    @Autowired
    private SpendRepository spendRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditConsumer.class);

    @Bean
    private KafkaConsumer<String, byte[]> getKafkaConsumer() {
        try {
            InputStream inputStream = new ClassPathResource("consumer.properties").getInputStream();
            Properties properties = new Properties();
            properties.load(inputStream);
            return new KafkaConsumer<String, byte[]>(properties);
        } catch (IOException exception) {
            LOGGER.error("Error loading properties {} ", exception);
            LOGGER.error("Shutting down. Ensure that consumer properties are read properly");
            System.exit(-1);
        }
        return null;
    }

    @PostConstruct
    public void initConsumer() {
        executorService = Executors.newFixedThreadPool(1);
        executorService.execute(() -> {
            consumer.subscribe(Collections.singletonList(topic));
            while (!isStopped.get()) {
                ConsumerRecords<String, byte[]> records = consumer.poll(1000);
                List<Spend> spendList = new ArrayList<>();
                for (ConsumerRecord<String, byte[]> record : records) {
                    try {
                        Spend spend = objectMapper.readValue(record.value(), Spend.class);
                        spendList.add(spend);
                    } catch (IOException ex) {
                        LOGGER.error("ERROR Parsing Spend data");
                        //Add record to failsafe logs for later processing.
                    }

                }
                for (Spend spend : spendList) {
                    if (spend.getSuccessful()) {
                        //TODO optimize this
                        EPayLaterUser user = userRepository.findOne(spend.getPhoneNumber());
                        user.setCreditUsed(user.getCreditUsed() + spend.getAmount());
                        userRepository.save(user);
                    }
                }

                if (spendList.size() > 0) {
                    spendRepository.save(spendList);
                    LOGGER.debug("Added Audit trail in DB");
                }

            }
            consumer.close();
        });

    }

    @PreDestroy
    public void cleanUp() {
        isStopped.set(true);
        executorService.shutdown();
    }


}
