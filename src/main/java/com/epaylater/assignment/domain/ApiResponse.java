package com.epaylater.assignment.domain;

public class ApiResponse<T> {

    private Status status;
    private T response;
    private String errorMessage;


    public ApiResponse(Status status, T response) {
        this.status = status;
        this.response = response;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
