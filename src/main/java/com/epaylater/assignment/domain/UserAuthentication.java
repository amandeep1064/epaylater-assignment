package com.epaylater.assignment.domain;

import com.epaylater.assignment.entities.EPayLaterUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.security.auth.Subject;
import java.util.Collection;

public class UserAuthentication implements Authentication {

    private EPayLaterUser epayLaterUser;

    public UserAuthentication(UserDetails userDetails) {
        this.epayLaterUser = (EPayLaterUser) userDetails;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return epayLaterUser.getPassword();
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return epayLaterUser;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public boolean isAuthenticated() {
        return epayLaterUser != null;
    }

    @Override
    public String getName() {
        return epayLaterUser.getPhoneNumber();
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }
}
