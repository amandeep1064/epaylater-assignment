package com.epaylater.assignment.service.impl;


import com.epaylater.assignment.component.KafkaProducerComponent;
import com.epaylater.assignment.component.RedisComponent;
import com.epaylater.assignment.entities.EPayLaterUser;
import com.epaylater.assignment.entities.Spend;
import com.epaylater.assignment.exceptions.NotEnoughCreditException;
import com.epaylater.assignment.service.SpendService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jedis.lock.JedisLock;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service
public class SpendServiceImpl implements SpendService {


    @Autowired
    private RedisComponent redisComponent;

    @Autowired
    private KafkaProducerComponent kafkaProducerComponent;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(SpendServiceImpl.class);


    @Override
    public Float processSpend(Spend spend) throws NotEnoughCreditException {
        Jedis jedis = redisComponent.getJedisInstance();
        EPayLaterUser user = getUserFromSecurityContext();
        enrichSpendPayload(spend, user);
        String key = String.format("credit_%s", user.getPhoneNumber());
        if (user.getCreditLimit() - user.getCreditUsed() > spend.getAmount()) {
            JedisLock lock = new JedisLock(jedis, user.getPhoneNumber(), 5000);
            try {
                lock.acquire();
                jedis.set(key, String.valueOf(user.getCreditLimit() - user.getCreditUsed() - spend.getAmount()));
                spend.setSuccessful(true);
                pubishSpendLogToKafka(spend);
                return user.getCreditLimit() - user.getCreditUsed() - spend.getAmount();
            } catch (InterruptedException exception) {
                throw new RuntimeException("Can't process your debit for time being"); } finally {
                lock.release();
                jedis.close();
            }
        } else {
            pubishSpendLogToKafka(spend);
            throw new NotEnoughCreditException("You don't have enough credit to process this successfully");
        }
    }

    private void pubishSpendLogToKafka(Spend spend) {
        try {
            kafkaProducerComponent.getKafkaProducer().send(new ProducerRecord<String, byte[]>("spend", objectMapper.writeValueAsBytes(spend)));
        } catch (IOException exception) {
            LOGGER.error("Couldn't publish to kafka Message is  :: {} due to  {} ", spend, exception);
            //TODO implement a failsafe approach write to file
        }
    }


    private void enrichSpendPayload(Spend spend, EPayLaterUser user) {
        spend.setTransactionId(UUID.randomUUID().toString());
        if (spend.getDate() == null) {
            spend.setDate(new Date());
        }
        spend.setPhoneNumber(user.getPhoneNumber());
    }

    private EPayLaterUser getUserFromSecurityContext() {
        EPayLaterUser user = (EPayLaterUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user;
    }
}
