package com.epaylater.assignment.service;

import com.epaylater.assignment.entities.Spend;
import com.epaylater.assignment.exceptions.NotEnoughCreditException;

public interface SpendService {

    public Float processSpend(Spend spend) throws NotEnoughCreditException;

}
