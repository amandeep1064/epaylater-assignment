package com.epaylater.assignment.controllers;


import com.epaylater.assignment.domain.ApiResponse;
import com.epaylater.assignment.entities.Spend;
import com.epaylater.assignment.domain.Status;
import com.epaylater.assignment.exceptions.NotEnoughCreditException;
import com.epaylater.assignment.service.SpendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class CreditController {

    @Autowired
    private SpendService spendService;

    @RequestMapping(value = "/spend", method = RequestMethod.POST)
    public ApiResponse<String> spendCredit(@RequestBody Spend spend, HttpServletRequest request) throws NotEnoughCreditException {
        Float remainingBalance = spendService.processSpend(spend);
        return new ApiResponse<>(Status.SUCCESS, String.format("Remaining Credit limit is %f", remainingBalance));
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public ResponseEntity<String> sayHello() {
        return new ResponseEntity<String>("hello", HttpStatus.OK);
    }
}
