package com.epaylater.assignment.controllers;

import com.epaylater.assignment.component.TokenHandler;
import com.epaylater.assignment.domain.ApiResponse;
import com.epaylater.assignment.domain.Status;
import com.epaylater.assignment.entities.EPayLaterUser;
import com.epaylater.assignment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ApiResponse<String> login(@RequestParam(value = "phone") String phoneNumber, @RequestParam(value = "password") String password, HttpServletRequest request, HttpServletResponse response) {
        EPayLaterUser user = userRepository.findOne(phoneNumber);
        boolean isValidPassword = encoder.matches(password, user.getPassword());
        if (isValidPassword) {
            String token = tokenHandler.createTokenForUser(user, request);
            response.setHeader("Authentication", token);
            return new ApiResponse<>(Status.SUCCESS, "Login Successful");
        } else
            throw new SecurityException("Wrong credentials. Please try again");
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ApiResponse<String> signUp(@RequestBody EPayLaterUser user, HttpServletRequest request) {
        EPayLaterUser userInDb = userRepository.findOne(user.getPhoneNumber());
        if (userInDb == null) {
            user.setPassword(encoder.encode(user.getPassword()));
            userRepository.save(user);
            return new ApiResponse<String>(Status.SUCCESS, "Signed Up Successfully");
        } else
            throw new RuntimeException("User already exists. Try logging in by using PhoneNumber and Password");
    }
}
