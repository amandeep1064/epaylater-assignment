package com.epaylater.assignment.controllers;

import com.epaylater.assignment.domain.ApiResponse;
import com.epaylater.assignment.domain.Status;
import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class CustomErrorController extends AbstractErrorController {


    public CustomErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping(value = "/error", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ApiResponse<String> handleError(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> errorAttributes = super.getErrorAttributes(request, false);
        return new ApiResponse<>(Status.FAILURE, errorAttributes.get("message").toString());
    }
}
