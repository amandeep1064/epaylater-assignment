package com.epaylater.assignment.filters;

import com.epaylater.assignment.component.TokenHandler;
import com.epaylater.assignment.domain.UserAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class StatelessAuthenticationFilter extends GenericFilterBean {

    private TokenHandler tokenHandler;

    public StatelessAuthenticationFilter(TokenHandler tokenHandler) {
        this.tokenHandler = tokenHandler;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (!httpRequest.getRequestURI().equals("/login") && !httpRequest.getRequestURI().equals("/signup")) {
            String token = httpRequest.getHeader("Authentication");
            if (StringUtils.isEmpty(token)) {
                throw new SecurityException("Token not Found. Please provide a valid token");
            }
            UserDetails details = tokenHandler.parseUserFromToken(token);
            SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(details));
            filterChain.doFilter(request, response);
            SecurityContextHolder.getContext().setAuthentication(null);
        } else {
            filterChain.doFilter(request, response);
        }
    }
}

